package com.hypernym.IOP.Model;

import android.media.Image;
import android.widget.ImageView;

public class Appliance {
   public String title;
   public ImageView icon_image;
   public Appliance(String title,ImageView icon_image){
       this.title=title;
       this.icon_image=icon_image;
   }

}
