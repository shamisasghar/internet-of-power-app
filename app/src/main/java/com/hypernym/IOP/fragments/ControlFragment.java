package com.hypernym.IOP.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.androidslidr.Slidr;
import com.hypernym.IOP.R;
import com.hypernym.IOP.toolbox.ToolbarListener;

public class ControlFragment extends Fragment implements Slidr.TextFormatter,Slidr.Listener,LinearLayout.OnClickListener{
    private ViewHolder mHolder;
    int slider_temperature,slider_fanspeed=0;
    View view;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
//        if (!EventBus.getDefault().isRegistered(this))
//            EventBus.getDefault().register(this);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle(" ");
        }
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       view=inflater.inflate(R.layout.fragment_control, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHolder = new ViewHolder(view);
        mHolder.slider.setListener(this);
        mHolder.slider1.setListener(this);
        mHolder.slider.setTextFormatter(this);
        mHolder.slider.setMin(0);
        mHolder.slider.setMax(32);
        mHolder.slider1.setMax(2);
        mHolder.slider1.addStep(new Slidr.Step("Normal", 1, Color.parseColor("#4E97D9"),Color.parseColor("#c82209")));
        mHolder.slider1.setTextMax("High");
        mHolder.slider1.setTextMin("Low");
        mHolder.linearLayout_humidity.setOnClickListener(this);
        mHolder.linearLayout_warm.setOnClickListener(this);
        mHolder.linearLayout_dry.setOnClickListener(this);
        mHolder.Text_temperature.setText("0");
        mHolder.Text_degree.setText((char) 0x00B0+"C");

        //   mHolder.button.setOnClickListener(this);
        //        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        //        toolbar.setOnClickListener(this);
    }

    @Override
    public String format(float value) {


        mHolder.slider1.setTextFormatter(new Slidr.TextFormatter() {
            @Override
            public String format(float value) {
                return "";
            }
        });
        return "";
    }

    @Override
    public void valueChanged(Slidr slidr, float currentValue) {
        switch (slidr.getId())
        {
            case R.id.slideure_1:
                slider_temperature=(int)currentValue;
                mHolder.Text_temperature.setText(slider_temperature+"");
              //  Toast.makeText(getContext(), ""+currentValue, Toast.LENGTH_SHORT).show();
                break;
            case R.id.slideure_2:
                slider_fanspeed=(int)currentValue;
            break;
        }
    }

    @Override
    public void bubbleClicked(Slidr slidr) {

    }


    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.layout_linear_humidity:
                Toast.makeText(getActivity(), "Humidity", Toast.LENGTH_SHORT).show();
                break;
            case R.id.layout_linear_warm:

                Toast.makeText(getContext(), "Warm", Toast.LENGTH_SHORT).show();
                break;
            case R.id.layout_linear_dry:
                Toast.makeText(getContext(), "Dry", Toast.LENGTH_SHORT).show();
                break;
        }
    }


    public static class ViewHolder {

        Slidr slider,slider1;
        TextView Text_appliance_title,Text_temperature,Text_degree;
        LinearLayout linearLayout_humidity,linearLayout_warm,linearLayout_dry;
        // Button button;
        public ViewHolder(View view) {
            //      button = (Button) view.findViewById(R.id.button);
            slider = (Slidr) view.findViewById(R.id.slideure_1);
            slider1 = (Slidr) view.findViewById(R.id.slideure_2);
            Text_appliance_title = (TextView) view.findViewById(R.id.txt_appliance_name);
            Text_degree = (TextView) view.findViewById(R.id.txt_degree_status);
            Text_temperature = (TextView) view.findViewById(R.id.txt_temperature);
            linearLayout_humidity = (LinearLayout) view.findViewById(R.id.layout_linear_humidity);
            linearLayout_warm = (LinearLayout) view.findViewById(R.id.layout_linear_warm);
            linearLayout_dry = (LinearLayout) view.findViewById(R.id.layout_linear_dry);


        }


    }

}