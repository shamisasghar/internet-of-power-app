package com.hypernym.IOP.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.hypernym.IOP.FrameActivity;
import com.hypernym.IOP.Model.Appliance;
import com.hypernym.IOP.Model.ApplianceCategory;
import com.hypernym.IOP.R;
import com.hypernym.IOP.adapter.ExpandableListAdapter;
import com.hypernym.IOP.toolbox.OnChildExpandableLayoutListener;
import com.hypernym.IOP.toolbox.ToolbarListener;
import com.hypernym.IOP.utils.ActivityUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Bilal Rashid on 10/10/2017.
 */

public class HomeFragment extends Fragment implements View.OnClickListener, OnChildExpandableLayoutListener {
    private ViewHolder mHolder;
    ExpandableListAdapter listAdapter;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    List<ApplianceCategory> categories;
    ApplianceCategory applianceCategory;
    View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
//        if (!EventBus.getDefault().isRegistered(this))
//            EventBus.getDefault().register(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle("APPLIANCES");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        // preparing list data
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHolder = new ViewHolder(view);
        prepareListData();
        listAdapter = new ExpandableListAdapter(getContext(), this, listDataHeader, listDataChild);
        mHolder.expListView.setAdapter(listAdapter);

        //   mHolder.button.setOnClickListener(this);
//        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
//        toolbar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        ActivityUtils.startActivity(getActivity(), FrameActivity.class, MonitorFragment.class.getName(), null);

    }

    public void PopulateUi(List<ApplianceCategory> applianceCategories) {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        List<String> appliancesubcategory = new ArrayList<String>();
//        for (int j = 0; j < applianceCategory.appliances.size(); j++) {
//            appliancesubcategory.add(applianceCategory.appliances.get(j).title);
//        }
        for (int i = 0; i < applianceCategories.size(); i++) {
            listDataHeader.add(applianceCategories.get(i).name);
            List<String> applicances=new ArrayList<String>();
            for (int j=0;j<applianceCategories.get(i).appliances.size();j++){
                applicances.add(applianceCategories.get(i).appliances.get(j).title);
            }
            listDataChild.put(listDataHeader.get(i),applicances ); // Header, Child data
        }
    }

    @Override
    public void onItemClick(View view, Object data, int child_position, int parent_position) {

        switch (view.getId()) {
            case R.id.txt_applicance_sub_category_name:

                ActivityUtils.startActivity(getActivity(), FrameActivity.class, AppliancesFragment.class.getName(), null);

                Toast.makeText(getContext(),
                        listDataHeader.get(parent_position)
                                + " : "
                                + listDataChild.get(
                                listDataHeader.get(parent_position)).get(
                                child_position), Toast.LENGTH_SHORT)
                        .show();
                break;
            case R.id.button_switch:
                Toast.makeText(getContext(), "Toggle"+listDataChild.get(listDataHeader.get(parent_position)).get(child_position), Toast.LENGTH_SHORT).show();
                break;
        }

    }

    public static class ViewHolder {

        ExpandableListView expListView;
        ImageView img_icon;

        // Button button;
        public ViewHolder(View view) {
            //      button = (Button) view.findViewById(R.id.button);
            expListView = (ExpandableListView) view.findViewById(R.id.Expandable_list);
            img_icon = (ImageView) view.findViewById(R.id.image_appliance);

        }

    }

    private void prepareListData() {
        categories = new ArrayList<>();

        applianceCategory = new ApplianceCategory();
        applianceCategory.name = "AIR CONDITIONERS";
        ImageView img_ac = new ImageView(getContext());
        img_ac.setImageResource(R.drawable.ic_ac);
        applianceCategory.appliances = new ArrayList<>();
        applianceCategory.appliances.add(new Appliance("Bed Room",img_ac));
        applianceCategory.appliances.add(new Appliance("Lounge",img_ac));
        applianceCategory.appliances.add(new Appliance("Servant Room",img_ac));
        categories.add(applianceCategory);

        applianceCategory = new ApplianceCategory();
        applianceCategory.name = "WATER DISPERNSERS";
        ImageView img_water_dispenser = new ImageView(getContext());
        img_water_dispenser.setImageResource(R.drawable.ic_water_dispenser);
        applianceCategory.appliances = new ArrayList<>();
        applianceCategory.appliances.add(new Appliance("water dispensers",img_water_dispenser));
        applianceCategory.appliances.add(new Appliance("water dispensers",img_water_dispenser));
        applianceCategory.appliances.add(new Appliance("water dispensers",img_water_dispenser));
        categories.add(applianceCategory);

        applianceCategory = new ApplianceCategory();
        applianceCategory.name = "REFRIGERATORS";
        ImageView img_ref = new ImageView(getContext());
        img_ref.setImageResource(R.drawable.ic_refrigerator);
        applianceCategory.appliances = new ArrayList<>();
        applianceCategory.appliances.add(new Appliance("refrigeators",img_ref));
        applianceCategory.appliances.add(new Appliance("refrigeators",img_ref));
        applianceCategory.appliances.add(new Appliance("refrigeators",img_ref));
        categories.add(applianceCategory);

        applianceCategory = new ApplianceCategory();
        applianceCategory.name = "DEEP FREEZERS";
        ImageView img_freezer = new ImageView(getContext());
        img_freezer.setImageResource(R.drawable.ic_freezer);
        applianceCategory.appliances = new ArrayList<>();
        applianceCategory.appliances.add(new Appliance("deep freezers",img_freezer));
        applianceCategory.appliances.add(new Appliance("deep freezers",img_freezer));
        applianceCategory.appliances.add(new Appliance("deep freezers",img_freezer));
        categories.add(applianceCategory);

        applianceCategory = new ApplianceCategory();
        applianceCategory.name = "WASHING MACHINES";
        ImageView img_machine = new ImageView(getContext());
        img_machine.setImageResource(R.drawable.ic_washing_machine);
        applianceCategory.appliances = new ArrayList<>();
        applianceCategory.appliances.add(new Appliance("washing machines",img_machine));
        applianceCategory.appliances.add(new Appliance("washing machines",img_machine));
        applianceCategory.appliances.add(new Appliance("washing machines",img_machine));
        categories.add(applianceCategory);

        PopulateUi(categories);

    }

}
