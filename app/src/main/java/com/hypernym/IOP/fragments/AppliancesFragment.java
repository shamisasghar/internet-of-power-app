package com.hypernym.IOP.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.hypernym.IOP.R;
import com.hypernym.IOP.toolbox.OnItemClickListener;
import com.hypernym.IOP.toolbox.ToolbarListener;

import lecho.lib.hellocharts.view.ColumnChartView;

public class AppliancesFragment extends Fragment implements View.OnClickListener {
    private ViewPager mViewPager;
    private SectionsPagerAdapter sectionsPagerAdapter;
    View view;
    public ViewHolder mHolder;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
//        if (!EventBus.getDefault().isRegistered(this))
//            EventBus.getDefault().register(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle(" ");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_appliances, container, false);
        sectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        mViewPager = (ViewPager) view.findViewById(R.id.container);
        mViewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.button_switch:
                if(mHolder.Text_switch_status.getText().equals("Switched OFF")) {
                    mHolder.Text_switch_status.setText("Switched ON");
                }
                else
                {
                    mHolder.Text_switch_status.setText("Switched OFF");
                }
                break;
        }

    }



    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    ControlFragment tab1 = new ControlFragment();
                    return tab1;
                case 1:
                    MonitorFragment tab2 = new MonitorFragment();
                    return tab2;
//                case 2:
//                    FailedJobFragment tab3 = new FailedJobFragment();
//                    return tab3;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    String var = "Controls";

                    return var;
                case 1:
                    return "Monitoring";

            }
            return null;
        }

    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHolder = new ViewHolder(view);
        mHolder.aSwitch.setOnClickListener(this);
        mHolder.Text_temperature.setText("20"+(char) 0x00B0+"C");
        mHolder.Text_switch_status.setText("Switched OFF");

    }

    public static class ViewHolder {

        ImageView Image_appliances;
        TextView Text_appliance_title, Text_switch_status, Text_temperature, Text_humidity;
        Switch aSwitch;

        public ViewHolder(View view) {
            Image_appliances = (ImageView) view.findViewById(R.id.image_appliance);
            Text_appliance_title = (TextView) view.findViewById(R.id.txt_appliance_title);
            Text_switch_status = (TextView) view.findViewById(R.id.txt_swith_status);
            Text_temperature = (TextView) view.findViewById(R.id.txt_temperature);
            Text_humidity = (TextView) view.findViewById(R.id.txt_humidity);
            aSwitch = (Switch) view.findViewById(R.id.button_switch);

        }

    }


}
