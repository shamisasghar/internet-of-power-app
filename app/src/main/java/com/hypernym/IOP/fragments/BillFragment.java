package com.hypernym.IOP.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.hypernym.IOP.R;
import com.hypernym.IOP.toolbox.ToolbarListener;
import com.hypernym.IOP.utils.ChartUtils;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.listener.ColumnChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.view.ColumnChartView;

public class BillFragment extends Fragment implements ColumnChartOnValueSelectListener {
    private ViewHolder mHolder;
    private boolean hasAxes = true;
    private boolean hasAxesNames = true;
    private boolean hasLabels = false;
    private boolean hasLabelForSelected = false;
    private int dataType = DEFAULT_DATA;
    private static final int DEFAULT_DATA = 0;
    private ColumnChartData data;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
//        if (!EventBus.getDefault().isRegistered(this))
//            EventBus.getDefault().register(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle(" ");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bill, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHolder = new ViewHolder(view);
        mHolder.chart.setOnValueTouchListener(this);
        toggleLabels();
//        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) mHolder.bottomNavigationView.getLayoutParams();
//        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
//        toolbar.setOnClickListener(this);
    }


    private void toggleLabels() {
        hasLabels = !hasLabels;

        if (hasLabels) {
            hasLabelForSelected = false;
            mHolder.chart.setValueSelectionEnabled(hasLabelForSelected);
        }

        generateData();
    }

    private void generateData() {
        int numSubcolumns = 2;
        int numColumns = 5;
        // Column can have many subcolumns, here by default I use 1 subcolumn in each of 8 columns.
        List<Column> columns = new ArrayList<Column>();
        List<SubcolumnValue> values;
        for (int i = 0; i < numColumns; ++i) {

            values = new ArrayList<SubcolumnValue>();
            for (int j = 0; j < numSubcolumns; ++j) {
                values.add(new SubcolumnValue((float) Math.random() * 50f + 5, ChartUtils.pickColor()));
            }

            Column column = new Column(values);
            column.setHasLabels(hasLabels);
            column.setHasLabelsOnlyForSelected(hasLabelForSelected);
            columns.add(column);
        }

        data = new ColumnChartData(columns);

        if (hasAxes) {
            Axis axisX = new Axis();
            Axis axisY = new Axis().setHasLines(true);
            if (hasAxesNames) {
                axisX.setName("Axis X");
                axisY.setName("Axis Y");
            }
            data.setAxisXBottom(axisX);
            data.setAxisYLeft(axisY);
        } else {
            data.setAxisXBottom(null);
            data.setAxisYLeft(null);
        }

        mHolder.chart.setColumnChartData(data);

    }

    @Override
    public void onValueSelected(int i, int i1, SubcolumnValue subcolumnValue) {
        Toast.makeText(getActivity(), "Selected: " + subcolumnValue, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onValueDeselected() {

    }

    public static class ViewHolder {


        private ColumnChartView chart;
        TextView Text_bill_month,Text_bill_status,Text_bill_amount;

        public ViewHolder(View view) {
            chart = (ColumnChartView) view.findViewById(R.id.chart);
            Text_bill_month = (TextView) view.findViewById(R.id.txt_bill_month);
            Text_bill_status = (TextView) view.findViewById(R.id.txt_bill_status);
            Text_bill_amount = (TextView) view.findViewById(R.id.txt_bill_amount);

        }

    }
}
