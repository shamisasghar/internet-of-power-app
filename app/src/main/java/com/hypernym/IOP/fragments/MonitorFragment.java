package com.hypernym.IOP.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.hypernym.IOP.R;
import com.hypernym.IOP.toolbox.ToolbarListener;

public class MonitorFragment extends Fragment implements View.OnClickListener,BottomNavigationView.OnNavigationItemSelectedListener{
    private ViewHolder mHolder;

    Fragment selectedFragment = null;
    View view;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
//        if (!EventBus.getDefault().isRegistered(this))
//            EventBus.getDefault().register(this);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarListener) {
            ((ToolbarListener) context).setTitle("Bottom");
        }
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_monitor, container, false);
        getfragment(new CurrentFragment());

        return view;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       mHolder = new ViewHolder(view);
       mHolder.bottomNavigationView.setOnNavigationItemSelectedListener(this);
//        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) mHolder.bottomNavigationView.getLayoutParams();
//        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
//        toolbar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_item1:
                item.setChecked(true);
                selectedFragment =  new CurrentFragment();
                break;

            case R.id.action_item2:
                item.setChecked(true);
                selectedFragment = new VoltageFragment();
                break;

            case R.id.action_item3:
                item.setChecked(true);
                selectedFragment = new GasFragment();
                break;

            case R.id.action_item4:
                item.setChecked(true);
                selectedFragment = new BillFragment();
                break;
            case R.id.action_item5:
                item.setChecked(true);
                selectedFragment = new TimeFragment();
                break;

        }
        getfragment(selectedFragment);


        return false;
    }

    public static class ViewHolder {


        BottomNavigationView bottomNavigationView;

        public ViewHolder(View view) {
            bottomNavigationView = (BottomNavigationView) view.findViewById(R.id.navigation);


        }

    }

    public void getfragment(Fragment fragment)
    {
        if(fragment!=null)
        {
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, fragment);
            transaction.commit();
        }
//        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.frame_layout, fragment);
//        transaction.commit();

    }
}
