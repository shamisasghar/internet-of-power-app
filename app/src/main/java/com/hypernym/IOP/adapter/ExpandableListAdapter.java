package com.hypernym.IOP.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.hypernym.IOP.R;
import com.hypernym.IOP.toolbox.OnChildExpandableLayoutListener;

import java.util.HashMap;
import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    TextView txtChild;
    ImageView icon_image;
    private List<String> _listDataHeader; // header titles
    private HashMap<String, List<String>> _listDataChild;
    private OnChildExpandableLayoutListener itemClickListener;
    private HashMap<Integer, boolean[]> mChildCheckStates;
    private ChildViewHolder childViewHolder;



    public ExpandableListAdapter(Context context, OnChildExpandableLayoutListener itemClickListener, List<String> listDataHeader,
                                 HashMap<String, List<String>> listChildData) {
        this._context = context;
        this.itemClickListener = itemClickListener;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.mChildCheckStates = new HashMap<Integer, boolean[]>();

    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, final ViewGroup parent) {


        final String childText = (String) getChild(groupPosition, childPosition);
       // final ImageView imageView = (ImageView) getChild(groupPosition, childPosition);
        final String item=_listDataHeader.get(childPosition);
        if (convertView == null) {
//            LayoutInflater infalInflater = (LayoutInflater) this._context
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                 convertView = infalInflater.inflate(R.layout.list_appliance_child, null);
         convertView=LayoutInflater.from(parent.getContext()).inflate(R.layout.list_appliance_child, parent, false);

            childViewHolder = new ChildViewHolder();
            childViewHolder.switchChild = (Switch) convertView
                    .findViewById(R.id.button_switch);
            convertView.setTag(R.layout.list_appliance_child, childViewHolder);


        }
        else
        {
            childViewHolder = (ChildViewHolder) convertView
                    .getTag(R.layout.list_appliance_child);
        }

        childViewHolder.switchChild.setOnCheckedChangeListener(null);

        if (mChildCheckStates.containsKey(groupPosition)) {

            boolean getChecked[] = mChildCheckStates.get(groupPosition);

            childViewHolder.switchChild.setChecked(getChecked[childPosition]);

        }
        else {

            boolean getChecked[] = new boolean[getChildrenCount(groupPosition)];

            mChildCheckStates.put(groupPosition, getChecked);
            childViewHolder.switchChild.setChecked(false);
        }
        childViewHolder.switchChild.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    boolean getChecked[] = mChildCheckStates.get(groupPosition);
                    getChecked[childPosition] = isChecked;
                    mChildCheckStates.put(groupPosition, getChecked);

                } else {

                    boolean getChecked[] = mChildCheckStates.get(groupPosition);
                    getChecked[childPosition] = isChecked;
                    mChildCheckStates.put(groupPosition, getChecked);
                }
            }
        });



        txtChild = (TextView) convertView.findViewById(R.id.txt_applicance_sub_category_name);
       // switchChild= (Switch) convertView.findViewById(R.id.button_switch);
     //   icon_image= (ImageView) convertView.findViewById(R.id.image_appliance);


        txtChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, item, childPosition,groupPosition);
            }
        });
        childViewHolder.switchChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, item, childPosition,groupPosition);
            }
        });


        txtChild.setText(childText);



        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) { return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {

          convertView=LayoutInflater.from(parent.getContext()).inflate(R.layout.list_appliance_parent, parent, false);
        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.txt_appliance_category_name);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public final class ChildViewHolder {
        Switch switchChild;
}

}
