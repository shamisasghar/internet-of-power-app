package com.hypernym.IOP.toolbox;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(View view, Object data, int position);
}
