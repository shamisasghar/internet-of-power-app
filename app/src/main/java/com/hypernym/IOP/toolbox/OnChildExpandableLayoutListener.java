package com.hypernym.IOP.toolbox;

import android.view.View;

public interface OnChildExpandableLayoutListener {
    void onItemClick(View view, Object data, int child_position,int parent_position);
}
